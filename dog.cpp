///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author Eduardo Kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {

Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender; 	 // not all dogs is name gender //
	species = "Canis Lupus"; // all dogs same (?) //
	hairColor = newColor;
	gestationPeriod = 64;
	name = newName;
}


const string Dog::speak() {
	return string( "Woof" );
}

void Dog::printInfo() {
	cout << "Dog Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

}

// namespace animalfarm
