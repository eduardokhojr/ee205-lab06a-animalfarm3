///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file rag.cpp
/// @version 1.0
///
/// Random Attribute Generators
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   3/09/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
using namespace std;
using namespace animalfarm;

int main() {
	srand(time(NULL));
	cout << "This is my test" << endl;
	
	cout << "Random Gender " << Animal::getRandomGender() << endl;
       
	cout << "Random Color " << Animal::getRandomColor() << endl;

	cout << "Random Bool " << boolalpha << Animal::getRandomBool() << endl;

	cout << "Random Weight " << Animal::getRandomWeight(0, 50000) << endl;

	cout << "Random Name " << Animal::getRandomName() << endl;

	for( int i = 0 ; i < 25; i++) {
		Animal *a = AnimalFactory::getRandomAnimal();
		cout << a->speak() << endl;
	}









}


























