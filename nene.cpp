///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( bool isMigratory, enum Color newColor, enum Gender newGender ){
	gender = newGender;
	species = "Branta sandvicensis";
	featherColor = newColor;
	isMigratory = true;
}

const string Nene::speak(){
	return string( "Nay, nay" );
}

void Nene::printInfo(){
	cout << "Nene" << endl;
	cout << "   Tag ID = [2202-A-802.1]" << endl;
	Bird::printInfo();
	cout << "   Is Migratory = [" << boolalpha << true << "]" << endl;
}
}//namespace animalfarm
