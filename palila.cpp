///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/18/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( bool isMigratory, enum Color newColor, enum Gender newGender ){
	gender = newGender;
	species = "Loxioides bailleui";
	featherColor = newColor;
	isMigratory = true;
}

const string Palila::speak(){
	return string( "Tweet" );
}

void Palila::printInfo(){
	cout << "Palila" << endl;
	cout << "   Where Found = [" << "Kapiolani Regional Park" << "]" << endl; 
	Bird::printInfo();
	cout << "   Is Migratory = [" << boolalpha << false << "]" << endl;
}
} //namespace animalfarm

